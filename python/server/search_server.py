import sys  # NOQA
import os
import json
from io import BytesIO
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir, os.pardir))
from flask import Flask, render_template, flash, request, url_for, redirect, session, send_file
from python.scripts import python_postgress
from python.dbconfig import DbConfig
from wtforms import Form, TextField, PasswordField, BooleanField, validators
from functools import wraps
import gc
from python.scripts import orders
from python.scripts import aisles
from python.scripts import department
from python.scripts import order_product_prior
from python.scripts import order_product_train
from python.scripts import products


application = Flask(__name__)
application.secret_key = "super secret key"


class RegistrationForm(Form):
    username = TextField('Username', [validators.Length(min=4, max=20)])
    email = TextField('Email Address', [validators.Length(min=6, max=50)])
    password = PasswordField('New Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    accept_tos = BooleanField('I accept the Terms of Service and Privacy Notice (updated Mar 22, 2018)', [validators.Required()])

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash("You need to login first")
            return redirect(url_for('login_page'))
    return wrap


@application.route('/')
def homepage():
    if 'logged_in' not in session:
        flash("Welcome to the landing page , you must login or signup to begin the data exploration")
    title = "Insta Cart Market Basket Analysis"
    try:
        return render_template("home.html")
    except Exception as e:
        return str(e)


@application.route('/graph')
def graph_Example(chartID = 'chart_ID', chart_type = 'line', chart_height = 500):
    chart = {"renderTo": chartID, "type": chart_type, "height": chart_height, }
    series = [{"name": 'Label1', "data": [1, 2, 3]}, {"name": 'Label2', "data": [4, 5, 6]}]
    title = {"text": 'My Title'}
    xAxis = {"categories": ['xAxis Data1', 'xAxis Data2', 'xAxis Data3']}
    yAxis = {"title": {"text": 'yAxis Label'}}
    return render_template('graph.html', chartID=chartID, chart=chart, series=series, title=title, xAxis=xAxis, yAxis=yAxis)


@application.route('/dashboard/')
@login_required
def dashboard():
    user= session['username']
    message="Welcome "+ str(user)+" , start exploring the data set"
    flash(message)
    return render_template("dashboard.html")


@application.route("/query/<string:table_name>/<int:num_of_records>/", methods=['GET'])
@login_required
def display(table_name,num_of_records):
    message="You are viewing "+ table_name+" table"
    flash(message)
    if table_name in ["aisles","department","products","orders","order_products__prior","order_products__train"]:
        records=python_postgress.execute_postgres_select_query(table_name,num_of_records)
        return render_template('index.html',output=json.loads(records))
    else:
        flash("Try again, table does not exists")
        return render_template("message.html")




@application.route('/login/', methods=["GET", "POST"])
def login_page():
    error = ''
    try:
        c, conn = python_postgress.get_connection()
        if request.method == "POST":
            query_string = 'SELECT password FROM users WHERE username = %s'
            username=request.form['username']
            c.execute(query_string, (username,))
            data=c.fetchone()['password']
            if request.form['password']== data:
                session['logged_in'] = True
                session['username'] = request.form['username']
                flash("You are now logged in")
                return redirect(url_for("dashboard"))
            else:
                error = "Invalid credentials, try again."
        gc.collect()
        return render_template("login.html", error=error)

    except Exception as e:
        error = "Invalid credentials, try again."
        return render_template("login.html", error=error)


@application.route('/register/', methods=["GET", "POST"])
def register_page():
    try:
        form = RegistrationForm(request.form)
        if request.method == "POST" and form.validate():
            username = str(form.username.data)
            email = str(form.email.data)
            password = str(form.password.data)
            c, conn= python_postgress.get_connection()
            query_string ='SELECT * FROM users WHERE username = %s'
            c.execute(query_string, (username,))
            if c.fetchone() != None:
                flash("That username is already taken, please choose another")
                return render_template('register.html', form=form)
            else:
                c.execute("INSERT INTO users (username, password, email, tracking) VALUES (%s, %s, %s, %s)",
                          ((username), (password), (email),
                           ("/demo/")))

                conn.commit()
                flash("Thanks for registering!")
                c.close()
                conn.close()
                gc.collect()

                session['logged_in'] = True
                session['username'] = username
                return redirect(url_for('dashboard'))
        return render_template("register.html", form=form)
    except Exception as e:
        return (str(e))


@application.errorhandler(404)
def page_not_found(e):
    return render_template("404.html")

@application.route("/run_query/")
def page_not_found():
    return render_template("query_form.html")


@application.route('/run_query/', methods=['POST'])
def my_form_post():
    user = session['username']
    query = request.form['query']
    if query.upper().startswith('DELETE') or query.upper().startswith('UPDATE') or query.upper().startswith('MODIFY') or query.upper().startswith('DROP') or query.upper().startswith('CREATE'):
        if user == "admin":
            if "USERS" in query.upper():
                flash("You are not allowed to view this information")
                return render_template("query_form.html")
            else:
                try:
                    records = python_postgress.run_user_query(query)
                    if records!=None:
                        return render_template('index.html', output=json.loads(records))
                    else:
                        flash("Operation performed Successfully")
                        return render_template("query_form.html")
                except Exception as ex:
                    flash("There is some error in your query, check it again")
                    return render_template("query_form.html")
        else:
            flash("Only admin can run delete, update, modify queries")
            return render_template("query_form.html")
    elif "USERS" in query.upper():
        flash("You are not allowed to view this information")
        return render_template("query_form.html")
    else:
        try:
            records=python_postgress.run_user_query(query)
            return render_template('index.html', output=json.loads(records))
        except Exception as e:
            flash("There is some error in your query, check it again")
            return render_template("query_form.html")

@application.route("/logout/")
@login_required
def logout():
    session.clear()
    flash("You have been logged out!")
    gc.collect()
    return redirect(url_for('homepage'))

## Products
@application.route("/plot_unique_count", methods=['GET'])
def plot_unique_count():
    plt=products.plot_departement_vs_product()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

@application.route("/plot_most_ordered_product", methods=['GET'])
def plot_most_ordered_product():
    plt=products.plot_most_ordered_product()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

##

## Orders
@application.route("/plot_count_of_orders", methods=['GET'])
def plot_count_of_orders():
    plt=orders.plot_count_of_orders()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')


@application.route("/plot_frequency_of_order", methods=['GET'])
def plot_frequency_of_order():
    plt=orders.plot_frequency_of_order()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')



@application.route("/plot_heat_map_of_frequency", methods=['GET'])
def plot_heat_map_of_frequency():
    plt=orders.plot_heat_map_of_frequency()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

@application.route("/plot_days_since_prior_order", methods=['GET'])
def plot_days_since_prior_order():
    plt=orders.plot_days_since_prior_order()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

##

## Order_product_train

@application.route("/plot_no_of_products_in_given_order", methods=['GET'])
def plot_no_of_products_in_given_order():
    plt=order_product_train.plot_no_of_products_in_given_order()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

@application.route("/plot_order_ratio_across_day_of_week", methods=['GET'])
def plot_order_ratio_across_day_of_week():
    plt=order_product_train.plot_order_ratio_across_day_of_week()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

@application.route("/plot_order_ratio_of_week_vs_hour", methods=['GET'])
def plot_order_ratio_of_week_vs_hour():
    plt=order_product_train.plot_order_ratio_of_week_vs_hour()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

##

##Order product prior

@application.route("/plot_product_occurance", methods=['GET'])
def plot_product_occurance():
    plt=order_product_prior.plot_product_occurance()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

@application.route("/plot_hours_of_order_in_day", methods=['GET'])
def plot_hours_of_order_in_day():
    plt=order_product_prior.plot_hours_of_order_in_day()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

@application.route("/plot_period_of_reorder", methods=['GET'])
def plot_period_of_reorder():
    plt=order_product_prior.plot_period_of_reorder()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

##

##Departement

@application.route("/plot_department_distribution", methods=['GET'])
def plot_department_distribution():
    plt=department.plot_department_distribution()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

@application.route("/plot_department_wise_order_ratio", methods=['GET'])
def plot_department_wise_order_ratio():
    plt=department.plot_department_wise_order_ratio()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')


@application.route("/plot_best_selling_departement", methods=['GET'])
def plot_best_selling_departement():
    plt=department.plot_best_selling_departement()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')


##

## Aisles
@application.route("/plot_aisles_ratio", methods=['GET'])
def plot_aisles_ratio():
    plt=aisles.plot_aisles_ratio()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

@application.route("/plot_add_to_cart_order", methods=['GET'])
def plot_add_to_cart_order():
    plt=aisles.plot_add_to_cart_order()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

@application.route("/plot_aisales_over_departement", methods=['GET'])
def plot_aisales_over_departement():
    plt=aisles.plot_aisales_over_departement()
    img = BytesIO()
    plt.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')

##


if __name__ == "__main__":
    application.run(host=DbConfig.HOST_NAME)
