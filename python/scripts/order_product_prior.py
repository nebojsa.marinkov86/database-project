import sys
import os
from python.scripts import python_postgress
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from matplotlib.ticker import FormatStrFormatter
import pandas as pd

sns.set()
color = sns.color_palette()

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir, os.pardir))
order_products__prior = python_postgress.generate_dataframe_from_table("order_products__prior")
aisles = python_postgress.generate_dataframe_from_table("aisles")
department = python_postgress.generate_dataframe_from_table("department")
products = python_postgress.generate_dataframe_from_table("products")
orders = python_postgress.generate_dataframe_from_table("orders")
order_products__train = python_postgress.generate_dataframe_from_table("order_products__train")

order_products__prior = pd.merge(order_products__prior, products, on='product_id', how='left')
order_products__prior = pd.merge(order_products__prior, aisles, on='aisle_id', how='left')
order_products__prior = pd.merge(order_products__prior, department, on='department_id', how='left')


def plot_product_occurance():
    plt.close()
    plt.figure(figsize=(12, 8))
    cnt_srs = order_products__prior['aisle'].value_counts().head(20)
    sns.barplot(cnt_srs.index, cnt_srs.values, alpha=0.8, color=color[5])
    plt.ylabel('Number of Occurrences', fontsize=12)
    plt.xlabel('Aisle', fontsize=12)
    plt.xticks(rotation='vertical')
    plt.tight_layout()
    return plt


def plot_hours_of_order_in_day():
    grouped = orders.groupby("order_id")["order_hour_of_day"].aggregate("sum").reset_index()
    grouped = grouped.order_hour_of_day.value_counts()
    plt.close()
    sns.set_style('darkgrid')
    f, ax = plt.subplots(figsize=(12, 8))
    sns.barplot(grouped.index, grouped.values)
    plt.ylabel('Number of orders', fontsize=13)
    plt.xlabel('Hours of order in a day', fontsize=13)
    plt.tight_layout()
    return plt


def plot_period_of_reorder():
    grouped = orders.groupby("order_id")["days_since_prior_order"].aggregate("sum").reset_index()
    grouped = grouped.days_since_prior_order.value_counts()
    plt.close()
    f, ax = plt.subplots(figsize=(12, 8))
    sns.barplot(grouped.index, grouped.values)
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    plt.ylabel('Number of orders', fontsize=13)
    plt.xlabel('Period of reorder', fontsize=13)
    plt.tight_layout()
    return plt
