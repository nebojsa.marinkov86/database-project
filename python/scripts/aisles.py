import sys
import os
from python.scripts import python_postgress
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

sns.set()
color = sns.color_palette()

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir, os.pardir))
order_products__prior = python_postgress.generate_dataframe_from_table("order_products__prior")
aisles = python_postgress.generate_dataframe_from_table("aisles")
department = python_postgress.generate_dataframe_from_table("department")
products = python_postgress.generate_dataframe_from_table("products")
orders = python_postgress.generate_dataframe_from_table("orders")
order_products__train = python_postgress.generate_dataframe_from_table("order_products__train")

order_products__prior = pd.merge(order_products__prior, products, on='product_id', how='left')
order_products__prior = pd.merge(order_products__prior, aisles, on='aisle_id', how='left')
order_products__prior = pd.merge(order_products__prior, department, on='department_id', how='left')
grouped_df = order_products__prior.groupby(["department_id", "aisle"])["reordered"].aggregate("mean").reset_index()


def plot_aisles_ratio():
    plt.close()
    fig, ax = plt.subplots(figsize=(12, 8))
    ax.scatter(grouped_df.reordered.values, grouped_df.department_id.values)
    for i, txt in enumerate(grouped_df.aisle.values):
        ax.annotate(txt, (grouped_df.reordered.values[i], grouped_df.department_id.values[i]), rotation=45, ha='center',
                    va='center', color='green')
    plt.xlabel('Reorder Ratio')
    plt.ylabel('department_id')
    plt.title("Reorder ratio of different aisles", fontsize=15)
    return plt


def plot_add_to_cart_order():
    order_products__prior["add_to_cart_order_mod"] = order_products__prior["add_to_cart_order"].copy()
    order_products__prior["add_to_cart_order_mod"].ix[order_products__prior["add_to_cart_order_mod"] > 70] = 70
    grouped_df = order_products__prior.groupby(["add_to_cart_order_mod"])["reordered"].aggregate("mean").reset_index()
    plt.close()
    plt.figure(figsize=(12, 8))
    sns.pointplot(grouped_df['add_to_cart_order_mod'].values, grouped_df['reordered'].values, alpha=0.8, color=color[2])
    plt.ylabel('Reorder ratio', fontsize=12)
    plt.xlabel('Add to cart order', fontsize=12)
    plt.title("Add to cart order - Reorder ratio", fontsize=15)
    plt.xticks(rotation='vertical')
    plt.tight_layout()
    return plt


def plot_aisales_over_departement():
    items = pd.merge(left=pd.merge(left=products, right=department, how='left'), right=aisles, how='left')
    grouped = items.groupby("aisle")["product_id"].aggregate({'Total_products': 'count'}).reset_index()
    grouped['Ratio'] = grouped["Total_products"].apply(lambda x: x / grouped['Total_products'].sum())
    grouped = grouped.sort_values(by='Total_products', ascending=False)[:20]
    grouped = grouped.groupby(['aisle']).sum()['Total_products'].sort_values(ascending=False)
    plt.close()
    f, ax = plt.subplots(figsize=(12, 8))
    plt.xticks(rotation='vertical')
    sns.barplot(grouped.index, grouped.values)
    plt.ylabel('Number of products', fontsize=13)
    plt.xlabel('Aisles', fontsize=13)
    plt.tight_layout()
    return plt
