import sys
import os
from python.scripts import python_postgress
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

sns.set()
color = sns.color_palette()

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir, os.pardir))
order_products__prior = python_postgress.generate_dataframe_from_table("order_products__prior")
aisles = python_postgress.generate_dataframe_from_table("aisles")
department = python_postgress.generate_dataframe_from_table("department")
products = python_postgress.generate_dataframe_from_table("products")
orders = python_postgress.generate_dataframe_from_table("orders")
order_products__train = python_postgress.generate_dataframe_from_table("order_products__train")

order_products__prior = pd.merge(order_products__prior, products, on='product_id', how='left')
order_products__prior = pd.merge(order_products__prior, aisles, on='aisle_id', how='left')
order_products__prior = pd.merge(order_products__prior, department, on='department_id', how='left')

grouped_df = order_products__prior.groupby(["department"])["reordered"].aggregate("mean").reset_index()


def plot_department_distribution():
    plt.close()
    plt.figure(figsize=(10, 10))
    temp_series = order_products__prior['department'].value_counts()
    labels = (np.array(temp_series.index))
    sizes = (np.array((temp_series / temp_series.sum()) * 100))
    plt.pie(sizes, labels=labels,
            autopct='%1.1f%%', startangle=200)
    plt.title("Departments distribution", fontsize=15)
    plt.tight_layout()
    return plt


def plot_department_wise_order_ratio():
    plt.close()
    plt.figure(figsize=(12, 8))
    sns.pointplot(grouped_df['department'].values, grouped_df['reordered'].values, alpha=0.8, color=color[2])
    plt.ylabel('Reorder ratio', fontsize=12)
    plt.xlabel('Department', fontsize=12)
    plt.title("Department wise reorder ratio", fontsize=15)
    plt.xticks(rotation='vertical')
    plt.tight_layout()
    return plt


def plot_best_selling_departement():
    items = pd.merge(left=pd.merge(left=products, right=department, how='left'), right=aisles, how='left')
    users_flow = orders[['user_id', 'order_id']].merge(order_products__train[['order_id', 'product_id']],
                                                       how='inner', left_on='order_id', right_on='order_id')

    users_flow = users_flow.merge(items, how='inner', left_on='product_id',
                                  right_on='product_id')
    grouped = users_flow.groupby("department")["order_id"].aggregate({'Total_orders': 'count'}).reset_index()
    grouped['Ratio'] = grouped["Total_orders"].apply(lambda x: x / grouped['Total_orders'].sum())
    grouped.sort_values(by='Total_orders', ascending=False, inplace=True)
    grouped = grouped.groupby(['department']).sum()['Total_orders'].sort_values(ascending=False)
    plt.close()
    f, ax = plt.subplots(figsize=(12, 8))
    plt.xticks(rotation='vertical')
    sns.barplot(grouped.index, grouped.values)
    plt.ylabel('Number of Orders', fontsize=13)
    plt.xlabel('Departments', fontsize=13)
    plt.tight_layout()
    return plt


