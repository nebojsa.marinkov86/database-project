import sys
import os
from python.scripts import python_postgress
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

sns.set()
color = sns.color_palette()

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir, os.pardir))

department = python_postgress.generate_dataframe_from_table("department")
products = python_postgress.generate_dataframe_from_table("products")
aisles = python_postgress.generate_dataframe_from_table("aisles")
order_products__train = python_postgress.generate_dataframe_from_table("order_products__train")
order_products__prior = python_postgress.generate_dataframe_from_table("order_products__prior")
order_products_all = pd.concat([order_products__train, order_products__prior], axis=0)


def merge_tables():
    goods = pd.merge(left=pd.merge(left=products, right=department, how='left'), right=aisles, how='left')
    # to retain '-' and make product names more "standard"
    goods.product_name = goods.product_name.str.replace(' ', '_').str.lower()
    return goods


def plot_departement_vs_product():
    plt.close()
    goods = merge_tables()
    goods.groupby(['department']).count()['product_id'].copy() \
        .sort_values(ascending=False).plot(kind='bar',
                                           # figsize=(12, 5),
                                           title='Departments: Product #')
    plt.tight_layout()
    return plt


def plot_most_ordered_product():
    grouped = order_products_all.groupby("product_id")["reordered"].aggregate({'Total_reorders': 'count'}).reset_index()
    grouped = pd.merge(grouped, products[['product_id', 'product_name']], how='left', on=['product_id'])
    grouped = grouped.sort_values(by='Total_reorders', ascending=False)[:10]
    grouped = grouped.groupby(['product_name']).sum()['Total_reorders'].sort_values(ascending=False)
    plt.close()
    sns.set_style('darkgrid')
    f, ax = plt.subplots(figsize=(12, 8))
    plt.xticks(rotation='vertical')
    sns.barplot(grouped.index, grouped.values)
    plt.ylabel('Number of Reorders', fontsize=13)
    plt.xlabel('Most ordered Products', fontsize=13)
    plt.tight_layout()
    return plt


# def plot_most_reordered_product():
#     grouped = order_products_all.groupby("product_id")["reordered"].aggregate(
#         {'reorder_sum': sum, 'reorder_total': 'count'}).reset_index()
#     grouped['reorder_probability'] = grouped['reorder_sum'] / grouped['reorder_total']
#     grouped = pd.merge(grouped, products[['product_id', 'product_name']], how='left', on=['product_id'])
#     grouped = grouped[grouped.reorder_total > 75].sort_values(['reorder_probability'], ascending=False)[:10]
#     grouped = grouped.groupby(['product_name']).sum()['reorder_probability'].sort_values(ascending=False)
#     plt.close()
#     sns.set_style('darkgrid')
#     f, ax = plt.subplots(figsize=(12, 8))
#     plt.xticks(rotation='vertical')
#     sns.barplot(grouped.index, grouped.values)
#     plt.ylabel('Number of Reorders', fontsize=13)
#     plt.xlabel('Most ordered Products', fontsize=13)
#     return plt

