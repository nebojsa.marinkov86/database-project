import sys
import os
from python.scripts import python_postgress
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

sns.set()
color = sns.color_palette()

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir, os.pardir))
order_products__train = python_postgress.generate_dataframe_from_table("order_products__train")
orders = python_postgress.generate_dataframe_from_table("orders")



def plot_no_of_products_in_given_order():
    plt.close()
    grouped_df = order_products__train.groupby("order_id")["add_to_cart_order"].aggregate("max").reset_index()
    cnt_srs = grouped_df.add_to_cart_order.value_counts()
    plt.figure(figsize=(12, 8))
    sns.barplot(cnt_srs.index, cnt_srs.values, alpha=0.8)
    plt.ylabel('Number of Occurrences', fontsize=12)
    plt.xlabel('Number of products in the given order', fontsize=12)
    plt.xticks(rotation='vertical')
    plt.tight_layout()
    return plt


def plot_order_ratio_across_day_of_week():
    order_products_train_df = pd.merge(order_products__train, orders, on='order_id', how='left')
    grouped_df = order_products_train_df.groupby(["order_dow"])["reordered"].aggregate("mean").reset_index()
    plt.close()
    plt.figure(figsize=(12, 8))
    sns.barplot(grouped_df['order_dow'].values, grouped_df['reordered'].values, alpha=0.8, color=color[3])
    plt.ylabel('Reorder ratio', fontsize=12)
    plt.xlabel('Day of week', fontsize=12)
    plt.title("Reorder ratio across day of week", fontsize=15)
    plt.xticks(rotation='vertical')
    plt.ylim(0.5, 0.7)
    plt.tight_layout()
    return plt


def plot_order_ratio_of_week_vs_hour():
    order_products__train = python_postgress.generate_dataframe_from_table("order_products__train")
    order_products__train = pd.merge(order_products__train, orders, on='order_id', how='left')
    plt.close()
    plt.figure(figsize=(12, 8))
    grouped_df = order_products__train.groupby(["order_dow", "order_hour_of_day"])["reordered"].aggregate(
        "mean").reset_index()
    grouped_df = grouped_df.pivot('order_dow', 'order_hour_of_day', 'reordered')
    sns.heatmap(grouped_df)
    plt.title("Reorder ratio of Day of week Vs Hour of day")
    plt.tight_layout()
    return plt
