import psycopg2
import pprint
import sys
import simplejson as json
import os
from psycopg2.extras import RealDictCursor
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir, os.pardir))

from python.dbconfig import DbConfig
from sqlalchemy import create_engine
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

sns.set()

color = sns.color_palette()

def get_connection():
    conn_string = "host='localhost' dbname='postgres' user="+DbConfig.POSTGRES_USERNAME+ " password="+DbConfig.POSTGRES_PASSWORD
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=RealDictCursor)
    return cursor, conn

def execute_postgres_select_query(table_name,num_of_records):
    cursor, conn= get_connection()
    cursor.execute("SELECT * FROM "+table_name+" LIMIT "+ str(num_of_records))
    print("Connected!\n")
    result=json.dumps(cursor.fetchall(), indent=2, use_decimal=True)
    conn.close()
    return result

def run_user_query(query):
    cursor, conn = get_connection()
    cursor.execute(query)
    if query.upper().startswith('DELETE') or query.upper().startswith('UPDATE') or query.upper().startswith('MODIFY') or query.upper().startswith('DROP') or query.upper().startswith('CREATE'):
        conn.commit()
        conn.close()
        return None
    else:
        result= (json.dumps(cursor.fetchall(), indent=2, use_decimal=True))
        conn.commit()
        conn.close()
        return result

def generate_dataframe_from_table(tableName):
    engine = create_engine('postgresql://postgres:infy@123@localhost:5432/postgres')
    query="select * from "+tableName+" LIMIT 100000"
    df = pd.read_sql_query(query, con=engine)
    return df

