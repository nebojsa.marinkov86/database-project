import os
from os import path
import logging
import sys

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir, os.pardir))
here = path.abspath(path.dirname(__file__))
LOG_LEVEL = logging.INFO
LOG_DIR = os.path.join(here, os.pardir, "log")

class DbConfig:
    HOST_NAME="0.0.0.0"
    POSTGRES_USERNAME="postgres"
    POSTGRES_PASSWORD = "infy@123"